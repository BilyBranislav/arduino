int voltage;
int brightness;
volatile boolean mozeZvuk;

long refreshInterval = 5000;
long lastRefreshTime = 0;

void setup() {
    Serial.begin(9600);
    mozeZvuk = true;
    
    attachInterrupt(0, RISING, notone);

    pinMode(13, OUTPUT); //piezo
    pinMode(11, OUTPUT);
    pinMode(10, OUTPUT);
    pinMode(6, OUTPUT);
    pinMode(5, OUTPUT); //ledky
}

void loop() {
  voltage = analogRead(A0);
  

  if(voltage - 768 > 0) {
    analogWrite(11, voltage - 768);
    analogWrite(10, 255);
    analogWrite(6, 255);
    analogWrite(5, 255);
  } else if(voltage - 512 > 0) {
    analogWrite(10, voltage - 512);
    analogWrite(6, 255);
    analogWrite(5, 255);
  } else if(voltage - 255 > 0) {
    analogWrite(6, voltage - 255);
    analogWrite(5, 255);
  } else if(voltage > 0) {
    analogWrite(5, voltage);
  } else if (voltage == 0) {
    if(millis() - lastRefreshTime >= refreshInterval) {
      lastRefreshTime += refreshInterval;
      for(int i = 0; i < 3; i++) {
        if(mozeZvuk) {
          tone(13, 1000, 400);
          delay(600);
        } else {
          noTone(13);
        }
      }
    }
  }
  if(voltage != 0) {
    mozeZvuk = true;
  }
}

void notone() {
  mozeZvuk = false;
  noTone(13);
}
