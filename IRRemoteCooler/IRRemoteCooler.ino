//This is property of Branislav Bily

#include <ir_Lego_PF_BitStreamEncoder.h>
#include <boarddefs.h>
#include <IRremoteInt.h>
#include <IRremote.h>

#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//From bildr article: http://bildr.org/2012/08/rotary-encoder-arduino/

#include "Wire.h" // For I2C
#include "LCD.h" // For LCD
#include "LiquidCrystal_I2C.h" // Added library*
//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7);

const int RECV_PIN = 7;
IRrecv irrecv(RECV_PIN);
decode_results results;

int frek = 200;
long per = 0;
int rpm = 100;
float realRPM = 0;

long impulse;
long lastImpulse = 0;
long difference;

void setup(){
  lcd.begin (16, 2); // 16 x 2 LCD module
  lcd.setBacklightPin(3, POSITIVE); // BL, BL_POL
  lcd.setBacklight(HIGH);
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  irrecv.enableIRIn();
  irrecv.blink13(true);
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(0, impulseReceiver, FALLING);
  lcd.home();
  lcd.setCursor(0, 0);
  lcd.print("set RPM: 0");
  lcd.setCursor(0, 1);
  lcd.print("real RPM: ");
}

void loop(){
  if (irrecv.decode(&results)){
    if(results.value == 16761405) {
      addRPM();
    } else if (results.value == 16720605) {
      slowRPM();
    }
    irrecv.resume();    
  }

  if(millis() >= per) {
    per += frek;
    float outputRPM = map(rpm, 0, 2600, 0, 255);
//    Serial.println(outputRPM);
    analogWrite(5, outputRPM);
    lcd.setCursor(10, 1);
    lcd.print("        ");
    lcd.setCursor(10, 1);
    lcd.print(realRPM);
  }
}

void addRPM() {
  if(rpm <= 2500) {
    rpm += 100;
    Serial.println(rpm);
    lcd.setCursor(9, 0);
    lcd.print("         ");
    lcd.setCursor(9, 0);
    lcd.print(rpm);
  }
}

void slowRPM() {
  if(rpm >= 100) {
    rpm -= 100;
    Serial.print(rpm);
    lcd.setCursor(9, 0);
    lcd.print("         ");
    lcd.setCursor(9, 0);
    lcd.print(rpm);
  }
}

void impulseReceiver() {
  impulse = millis();
  difference = (impulse - lastImpulse) * 2;
  lastImpulse = impulse;
  realRPM = 1000 / difference * 60;
  
}
