int analogVstup;
int current;
int currentLimit = 400;
int led = 13;
int piezo = 8;
boolean zvuk = false;


void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(piezo, OUTPUT);
}

void loop(){
   analogVstup = analogRead(A0);
   current = map(analogVstup, 0, 1023, 0, 5000);
   if(current > currentLimit) {
    digitalWrite(led, 1);
    if(!zvuk) {
      tone(piezo, 1000, 200);
      zvuk = true;
    }
    Serial.println("Current limited"); 
   } else {
    digitalWrite(led, 0);
    Serial.print("current = "); Serial.print(current); Serial.println(" mA");
    zvuk = false;
   }
}
