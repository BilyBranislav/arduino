#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//From bildr article: http://bildr.org/2012/08/rotary-encoder-arduino/

#include "Wire.h" // For I2C
#include "LCD.h" // For LCD
#include "LiquidCrystal_I2C.h" // Added library*
//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the default I2C bus address of the backpack-see article
int pos = 6;

void setup() {
  lcd.begin (16, 2); // 16 x 2 LCD module
  lcd.setBacklightPin(3, POSITIVE); // BL, BL_POL
  lcd.setBacklight(HIGH);
  Serial.begin (9600);
  pinMode(10, OUTPUT);
  digitalWrite(10, 1);
}

void loop() {
  if(Serial.available() > 0) {
    char tlacidlo = Serial.read();
    if(tlacidlo == 'a' && pos > 0) {
        pos--;
    } else if(tlacidlo == 'd' && pos < 11) {
      pos++;
    }
  }
  lcd.home();
  lcd.print("                ");
  lcd.setCursor(pos, 0);
  lcd.print("Janko"); // Custom text
  lcd.setCursor(0, 1);
  lcd.print("                ");
  lcd.setCursor (pos, 1);
  lcd.print("Hasko");
}
