#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//From bildr article: http://bildr.org/2012/08/rotary-encoder-arduino/

#include "Wire.h" // For I2C
#include "LCD.h" // For LCD
#include "LiquidCrystal_I2C.h" // Added library*
//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the default I2C bus address of the backpack-see article
int rok = 2019;
int perioda = 1000;
long mil = 0;
int stav = 1;

void setup() {
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
  pinMode(12, INPUT);
  pinMode(13, INPUT);
  attachInterrupt(0, vlavo, RISING);
  attachInterrupt(1, vpravo, RISING);
  Serial.begin(9600);
  lcd.begin (16, 2); // 16 x 2 LCD module
  lcd.setBacklightPin(3, POSITIVE); // BL, BL_POL
  lcd.setBacklight(HIGH);
}

void loop() {
  if(millis()  > mil) {
    mil += perioda;
    lcd.setBacklight(stav);
    stav = 1 - stav;
  }
  if(digitalRead(12)) {
    delay(100);
    rok++;
    lcd.print("                ");
  } else if(digitalRead(13)) {
    delay(100);
    rok--;
    lcd.print("                ");
  }
  lcd.home();
  lcd.setCursor(4, 0);
  lcd.print("PF ");
  lcd.setCursor(7, 0);
  lcd.print(rok);
}


void vlavo() {
  if(perioda != 200) {
    perioda -= 200;
  }
}

void vpravo() {
  if(perioda != 3000) {
    perioda += 200;
  }
}
