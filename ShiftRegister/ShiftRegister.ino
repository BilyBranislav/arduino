// pin připojen na RCLK registru (12)
int latchPin = 9;
//Pin connected to SRCLK registru (11)
int clockPin = 10;
////Pin připojen na Data pin registu (14)
int dataPin = 8;
 
 
void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  Serial.begin(9600);
}
 
void loop() {
 }

 void tenisoveLopticky() {
  byte data [] = {
    B10000000,
    B11000000,
    B11100000,
    B11110000,
    B11111000,
    B11111100,
    B11111110,
    B11111111,
    B00000000
  };

  for (int index = 0; index < sizeof(data); index = index + 1) {
    digitalWrite(latchPin, LOW);
    pocitadlo(dataPin, clockPin, MSBFIRST, data[index]);
    digitalWrite(latchPin, HIGH);
    delay(200);
    }
 }

 void blikanieHorneDolne() {
  long per = 0;
  int frek = 500;
  int index = 1;

  byte data [] = {
    B11110000,
    B00001111
  };
  if(millis() >= per + frek) {
    per += frek;
    index = 1 - index;
    digitalWrite(latchPin, LOW);
    pocitadlo(dataPin, clockPin, MSBFIRST, data[index]);
    digitalWrite(latchPin, HIGH);
    delay(200);
  }
 }


  void obojsmerne() {
    byte data[] =
  {
  B10000001,
  B10000001,
  B01000010,
  B00100100,
  B00011000,
  B00100100,
  B01000010,
  B10000001,
  B10000001
  };
    for (int index = 0; index < sizeof(data); index = index + 1) {
    digitalWrite(latchPin, LOW);
    pocitadlo(dataPin, clockPin, MSBFIRST, data[index]);
    digitalWrite(latchPin, HIGH);
    delay(200);
  }
 }

 void pohybujucaLEDKA() {
  byte data[] =
    {
    B10000000,
    B01000000,
    B00100000,
    B00010000,
    B00001000,
    B00000100,
    B00000010,
    B00000001,
    B00000010,
    B00000100,
    B00001000,
    B00010000,
    B00100000,
    B01000000
   };
 
  for (int index = 0; index < sizeof(data); index++) {
    digitalWrite(latchPin, LOW);
    pocitadlo(dataPin, clockPin, MSBFIRST, data[index]);  
    digitalWrite(latchPin, HIGH);
    delay(50);
  }
 }


 void pocitadlo(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val){
     uint8_t i;
     for (i = 0; i < 8; i++)  {
           if (bitOrder == LSBFIRST) {
            digitalWrite(dataPin, !!(val & (1 << i)));
           }else {
              digitalWrite(dataPin, !!(val & (1 << (7 - i))));
           }  
           digitalWrite(clockPin, HIGH);
           digitalWrite(clockPin, LOW);            
     }
}
