#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//From bildr article: http://bildr.org/2012/08/rotary-encoder-arduino/

#include "Wire.h" // For I2C
#include "LCD.h" // For LCD
#include "LiquidCrystal_I2C.h" // Added library*
//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the default I2C bus address of the backpack-see article
int frek1 = 1000;
int frek2 = 200;
long per1 = 0;
long per2 = 0;
int minutes = 0;
int seconds = 0;


void setup() {
  // Set off LCD module
  lcd.begin (16, 2); // 16 x 2 LCD module
  lcd.setBacklightPin(3, POSITIVE); // BL, BL_POL
  lcd.setBacklight(HIGH);
  Serial.begin (9600);
  pinMode(10, OUTPUT);
  digitalWrite(10, 1);
}

void loop() {
  if(millis() >= per1) {
    per1 += frek1;
    seconds += 1;
    if(seconds == 60) {
      seconds = 0;
      minutes++;
    }
    lcd.setCursor(6, 0);
    if(minutes < 10) {
      lcd.print("0");
      lcd.setCursor(7, 0);
    }
    lcd.print(minutes);
    lcd.setCursor(8, 0);
    lcd.print(":");
    lcd.setCursor(9, 0);
    if(seconds < 10) {
      lcd.print("0");
      lcd.setCursor(10, 0);
    }
    lcd.print(seconds);
  }

  if(millis() >= per2) {
    per2 += frek2;
    int nacitanie = analogRead(A0);
    int napatie = map(nacitanie, 0, 1021, 0, 5000);
    Serial.println(napatie);
    lcd.setCursor(0, 1);
    lcd.print("                ");
    lcd.setCursor(5, 1);
    if(napatie < 10) {
      lcd.setCursor(8, 1); 
    } else if(napatie < 100) {
      lcd.setCursor(7, 1);
    } else if(napatie < 1000) {
      lcd.setCursor(6, 1);
    }
    lcd.print(napatie);
    lcd.setCursor(9, 1);
    lcd.print("mV");
  }
}
