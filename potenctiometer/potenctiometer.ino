int LED = 12;
int potenciometer;
double napatie;
double prevNapatie = 0;
double percenta;
long per = 0;
int frek = 500;


void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(9600);
}

void loop() {
    potenciometer = analogRead(A0);
    napatie = map(potenciometer, 0, 1023, 0, 500);
    napatie = napatie / 100;
    percenta = map(potenciometer, 0, 1023, 0, 10000);
    percenta = percenta / 100;
    analogWrite(11, potenciometer / 4);
    
    if(millis() >= per){
      if(prevNapatie != napatie) {
        prevNapatie = napatie;
        per += frek;
        vypis();
      } else {
        per += frek;
      }
    }
}

void vypis(){
      Serial.print("U= ");
      Serial.print(napatie);
      Serial.print(" V \t");
      Serial.print(percenta);
      Serial.println("%");
}
