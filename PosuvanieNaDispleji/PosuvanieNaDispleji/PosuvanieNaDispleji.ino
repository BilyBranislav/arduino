#include <LCD.h>
#include <LiquidCrystal_I2C.h>

//From bildr article: http://bildr.org/2012/08/rotary-encoder-arduino/

#include "Wire.h" // For I2C
#include "LCD.h" // For LCD
#include "LiquidCrystal_I2C.h" // Added library*
//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the default I2C bus address of the backpack-see article
int x = 0;
int y = 0;

int frek = 200;
long per = 0;

int values[4][5] = {
  {11, 12, 13, 14, 15},
  {21, 22, 23, 24, 25},
  {31, 32, 33, 34, 35},
  {41, 42, 43, 44, 45} 
};


void setup() {
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
  pinMode(12, INPUT); //dole
  pinMode(13, INPUT); //hore
  pinMode(11, INPUT); //vlavo
  pinMode(10, INPUT); //vpravo
  Serial.begin(9600);
  lcd.begin (16, 2); // 16 x 2 LCD module
  lcd.setBacklightPin(3, POSITIVE); // BL, BL_POL
  lcd.setBacklight(HIGH);
  vypis();
}

void loop() {
  if(millis() >= per) {
    per = per + frek;
    if(digitalRead(12)) {
      pridatY();
      vypis();
    } else if (digitalRead(13)) {
      odobratY();
      vypis();
    } else if (digitalRead(11)) {
      odobratX();
      vypis();
    } else if (digitalRead(10)) {
      pridatX();
      vypis();
    }
    Serial.print(x);
    Serial.print(" ");
    Serial.print(y);
    Serial.print(" ");
    Serial.println(values[x][y]);
  }
}

void vypis() {
    lcd.setCursor(0, 0);
    lcd.print("                "); 
    lcd.setCursor(0, 1);
    lcd.print("                "); 
    lcd.setCursor(0, 0);
    lcd.print("Pozicia: ");
    lcd.setCursor(9, 0);
    lcd.print("[");
    lcd.setCursor(10, 0);
    lcd.print(x);
    lcd.setCursor(11, 0);
    lcd.print("]");
    lcd.setCursor(12, 0);
    lcd.print("[");
    lcd.setCursor(13, 0);
    lcd.print(y);
    lcd.setCursor(14, 0);
    lcd.print("]");
    lcd.setCursor(0, 1);
    lcd.print("Hodnota: ");
    lcd.setCursor(10, 1);
    lcd.print(values[x][y]); 
}

void pridatY() {
  if(y < 4) {
    y = y + 1;
  }
}

void odobratY() {
  if(y > 0) {
    y = y - 1;
  }
}

void pridatX() {
  if(x < 3) {
    x = x + 1;
  }
}

void odobratX() {
  if(x > 0) {
    x = x - 1;
  }
}
